let nav = 0;
let paspaustaAnt = null;
let events = localStorage.getItem('events') ? JSON.parse(localStorage.getItem('events')) : [];

const kalendorius = document.getElementById('kalendorius');
const dienosModalas = document.getElementById('dienosModalas');
const uzsklandosFonas = document.getElementById('modalBackDrop');

const savaitesDienos = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

function openModal(date) {
    paspaustaAnt = date;

    const eventForDay = events.find(e => e.date === paspaustaAnt);

    if (eventForDay) {
        document.getElementById('eventText').innerText = eventForDay.title;
    } else {
        dienosModalas.style.display = 'block';
    }
    uzsklandosFonas.style.display = 'block';
}

function load() {
    const dt = new Date();

    if (nav !== 0) {
        dt.setMonth(new Date().getMonth() + nav);
    }

    const day = dt.getDate();
    const month = dt.getMonth();
    const year = dt.getFullYear();

    const pirmaMenDiena = new Date(year, month, 1);
    const menDienos = new Date(year, month + 1, 0).getDate();

    const dateString = pirmaMenDiena.toLocaleDateString('en-us',
        {
            weekday: 'long',
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
        })

    const tusciaDiena = savaitesDienos.indexOf(dateString.split(', ')[0]);

    document.getElementById('menuo').innerText = `${dt.toLocaleDateString('en-us', {month: 'long'})} ${year}`

    kalendorius.innerHTML = "";

    var gimtadienioDiena = parseInt((localStorage.getItem("gimtadienisLocalStorage")).slice(-2));
    var gimtadienioMenuo = parseInt(((localStorage.getItem("gimtadienisLocalStorage")).slice(-5)).substring(0, 2));
    var gimtadienioVardas = localStorage.getItem("vardasLocalStorage");
    var gimtadienioPastas = localStorage.getItem("pastasLocalStorage");

    for (let i = 1; i <= tusciaDiena + menDienos; i++) {
        const daySquare = document.createElement('div');
        daySquare.classList.add('day');

        const dayString = `${month + 1}/${i - tusciaDiena}/${year}`;

        if (i > tusciaDiena) {
            daySquare.innerText = i - tusciaDiena;
            const eventForDay = events.find(e => e.date === dayString);

            if (i - tusciaDiena === day && nav === 0) {
                daySquare.id = 'siandien';
            }

            if (i - tusciaDiena === gimtadienioDiena && month + 1 === gimtadienioMenuo) {
                daySquare.id = 'gimtadienis';
                img = document.getElementById("bDayPic");
                img.src = localStorage.getItem("gimtadienisNuotraukaLocalStorage");

                p = document.getElementById("bDaydescription");
                p.innerHTML = localStorage.getItem("gimtadienioDescriptionLocalStorage");

                daySquare.innerText = i - tusciaDiena + "\n" + "V: " + gimtadienioVardas + "\n" + "A: " + getAge((localStorage.getItem("gimtadienisLocalStorage")).replace(/-/g, "/")) + "\n" + "E: " + gimtadienioPastas + "\n";

                daySquare.addEventListener('click', () => openModal(dayString));

            }
        } else {
            daySquare.classList.add('tusciaDiena');
        }
        kalendorius.appendChild(daySquare);
    }
}

function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function closeModal() {
    dienosModalas.style.display = 'none';
    uzsklandosFonas.style.display = 'none';
    paspaustaAnt = null;
    load();
}

function initButtons() {
    document.getElementById('nextButton').addEventListener('click', () => {
        nav++;
        load();
    });

    document.getElementById('backButton').addEventListener('click', () => {
        nav--;
        load();
    });

    document.getElementById('cancelButton').addEventListener('click', closeModal);
}

initButtons();
load();