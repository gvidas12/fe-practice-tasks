function getDate() {
    return document.getElementById('dateSearch').value;
}

var gimtadienisLocalStorage = localStorage.getItem("gimtadienisLocalStorage");

async function call() {
    let request = '';
    fetch("https://api.nasa.gov/planetary/apod?&api_key=caS9OBpA4cojqwJwYETsANgGhn4DcNDFtbAg208X").then(response => {
        return response.json();
    }).then(async function (myJSON) {
        request = `https://api.nasa.gov/planetary/apod?date=` + gimtadienisLocalStorage + '&api_key=' + 'caS9OBpA4cojqwJwYETsANgGhn4DcNDFtbAg208X';


        await fetch(request).then(function (response) {
            return response.json();
        }).then(function (myJSON) {

            p = document.getElementById("description");
            p.innerHTML = myJSON.explanation;

            img = document.getElementById("spacePic");
            img.src = myJSON.url;

            localStorage.setItem("gimtadienisNuotraukaLocalStorage", img.src);
            localStorage.setItem("gimtadienioDescriptionLocalStorage", p.innerHTML);
        })
    });
}