var selectedRow = null;

function onFormSubmit() {
    var formData = readFormData();
    if (selectedRow == null) {
        insertNewRecord(formData);
    } else {
        udpateRecord(formData);
    }
    resetForm();
}

function readFormData() {
    var formData = {};
    formData["vardas"] = document.getElementById("vardas").value;
    formData["failas"] = document.getElementById("failas").value;
    formData["gimtadienis"] = document.getElementById("gimtadienis").value;
    formData["pastas"] = document.getElementById("pastas").value;
    formData["telefonas"] = document.getElementById("telefonas").value;
    return formData;
}

function insertNewRecord(data) {
    var table = document.getElementById("sarasas").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.vardas;
    localStorage.setItem("vardasLocalStorage", data.vardas);

    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.failas;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.gimtadienis;
    localStorage.setItem("gimtadienisLocalStorage", data.gimtadienis);

    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.pastas;
    localStorage.setItem("pastasLocalStorage", data.pastas);

    cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.telefonas
    cell6 = newRow.insertCell(5);
    cell6.innerHTML = `<a onClick="onEdit(this)">Redaguoti</a>
                       <a onClick="onDelete(this)">Salinti</a>`;
}

function resetForm() {
    document.getElementById("vardas").value = "";
    document.getElementById("failas").value = "";
    document.getElementById("gimtadienis").value = "";
    document.getElementById("pastas").value = "";
    document.getElementById("telefonas").value = "";
    selectedRow = null;
}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("vardas").value = selectedRow.cells[0].innerHTML;
    document.getElementById("gimtadienis").value = selectedRow.cells[2].innerHTML;
    document.getElementById("pastas").value = selectedRow.cells[3].innerHTML;
    document.getElementById("telefonas").value = selectedRow.cells[4].innerHTML;
}

function udpateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.vardas;
    selectedRow.cells[1].innerHTML = formData.failas;
    selectedRow.cells[2].innerHTML = formData.gimtadienis;
    selectedRow.cells[3].innerHTML = formData.pastas;
    selectedRow.cells[4].innerHTML = formData.telefonas;
}

function onDelete(td) {
    row = td.parentElement.parentElement;
    document.getElementById("sarasas").deleteRow(row.rowIndex);
    resetForm();
}

